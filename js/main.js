//console.log("hello world");

const technologiesBody = document.getElementById("technologiesBody");
const tableEls = document.querySelectorAll("td");
// indexes:
/*
0 => keine
1 => Jira
2 => Git 
*/

for (let i = 0; i < tableEls.length; i++) {
  tableEls[i].addEventListener("click", (e) => {
    //console.log(i);
    update(i);
  });
}
const keinText = "";

const textJira = `
<h2>Was ist Jira?</h2>
<p>Jira ist eine leistungsstarke Projektmanagement-Software, die von Atlassian entwickelt wurde. Sie bietet umfangreiche Funktionen zur Verwaltung von Projekten, Aufgaben und Arbeitsabläufen. Jira ist bekannt für seine Flexibilität und Anpassbarkeit, was es zu einem beliebten Werkzeug für verschiedene Teams und Branchen macht.</p>

<h2>Wie nutzt man Jira in DevOps?</h2>
<p>In DevOps-Umgebungen kann Jira eine zentrale Rolle spielen, um die Zusammenarbeit zwischen Entwicklung, Betrieb und anderen Teams zu erleichtern. Hier sind einige Möglichkeiten, wie Jira in DevOps genutzt werden kann:</p>
<ul>
    <li>Verfolgung von Aufgaben und Arbeitsabläufen: Jira ermöglicht es Teams, Aufgaben, Fehler und Änderungsanforderungen zu verfolgen und den Fortschritt in Echtzeit zu überwachen.</li>
    <li>Integration von Entwicklungs- und Betriebsprozessen: Jira kann nahtlos mit anderen DevOps-Tools wie Versionskontrollsystemen, CI/CD-Pipelines und Überwachungsplattformen integriert werden, um einen reibungslosen Arbeitsablauf zu gewährleisten.</li>
    <li>Automatisierung von Workflows: Durch die Anpassung von Workflows in Jira können Teams automatisierte Prozesse implementieren, um Aufgaben schneller zu erledigen und Engpässe zu reduzieren.</li>
    <li>Echtzeit-Kommunikation und Zusammenarbeit: Jira bietet Funktionen für Kommentare, Benachrichtigungen und Diskussionen, die die Zusammenarbeit zwischen Teammitgliedern verbessern und die Transparenz über den Projektstatus erhöhen.</li>
</ul>

<h2>Wie bindet man Jira in ein Projekt ein? (Schritt-für-Schritt-Anleitung)</h2>
<ol>
    <li>Einrichten eines Jira-Projekts: Melden Sie sich bei Jira an und erstellen Sie ein neues Projekt. Wählen Sie den Projekttyp aus, der am besten zu Ihrem Projekt passt (z. B. Scrum, Kanban oder klassisch).</li>
    <li>Konfigurieren der Arbeitsabläufe: Passen Sie die Arbeitsabläufe in Jira an Ihre spezifischen Anforderungen an. Definieren Sie die verschiedenen Status, Transitions und Zustände, die Ihre Aufgaben durchlaufen sollen.</li>
    <li>Hinzufügen von Benutzern und Teams: Laden Sie Teammitglieder in Ihr Jira-Projekt ein und organisieren Sie sie in Teams oder Gruppen, je nach Bedarf.</li>
    <li>Erstellen von Aufgaben und Tickets: Erstellen Sie Aufgaben, Fehlerberichte und Änderungsanforderungen in Jira und weisen Sie sie den entsprechenden Teammitgliedern zu.</li>
    <li>Integration mit anderen Tools: Integrieren Sie Jira mit anderen DevOps-Tools, die Sie verwenden, wie z. B. Versionskontrollsystemen (z. B. Git), CI/CD-Pipelines (z. B. Jenkins) und Überwachungsplattformen (z. B. Prometheus).</li>
    <li>Anpassen von Dashboards und Berichten: Erstellen Sie Dashboards und Berichte in Jira, um den Projektfortschritt zu überwachen, Engpässe zu identifizieren und die Leistung des Teams zu analysieren.</li>
    <li>Schulung und Unterstützung: Bieten Sie Schulungen und Unterstützung für Ihr Team an, um sicherzustellen, dass alle mit der Verwendung von Jira vertraut sind und das Beste aus dem Tool herausholen können.</li>
</ol>

<p>Indem Sie Jira in Ihr DevOps-Projekt integrieren und die oben genannten Schritte befolgen, können Sie die Zusammenarbeit verbessern, den Projektfortschritt verfolgen und ein effizientes Arbeitsumfeld schaffen.</p>
`;

const textGitBranchingMerging = `
<h2>Was ist Git Branching und Merging?</h2>
<p>Git Branching und Merging sind grundlegende Konzepte im Bereich der Versionsverwaltung, insbesondere bei der Verwendung von Git. Hier sind einige grundlegende Erklärungen:</p>
<ul>
    <li><strong>Branching:</strong> Branching bezieht sich auf das Erstellen eines Zweigs (Branch) in einem Git-Repository, um unabhängige Arbeitsstränge zu erstellen. Jeder Branch kann unabhängig entwickelt werden, ohne die Hauptentwicklungslinie zu beeinträchtigen.</li>
    <li><strong>Merging:</strong> Merging bezieht sich auf das Zusammenführen von Änderungen aus einem Branch in einen anderen. Dies ermöglicht es, die in einem Zweig durchgeführten Arbeiten in den Hauptentwicklungszweig zu integrieren, um eine konsistente Codebasis sicherzustellen.</li>
</ul>

<h2>Wie nutzt man Git Branching und Merging in DevOps?</h2>
<p>In DevOps-Umgebungen sind Git Branching und Merging entscheidend für einen effizienten Arbeitsablauf. Hier sind einige Aspekte, wie sie genutzt werden können:</p>
<ul>
    <li><strong>Feature-Entwicklung:</strong> Entwickler können separate Branches für die Implementierung neuer Funktionen erstellen, ohne die Hauptentwicklungslinie zu beeinträchtigen. Nach Abschluss der Entwicklung können die Änderungen in den Hauptentwicklungszweig gemerged werden.</li>
    <li><strong>Hotfixes:</strong> Bei der Behebung von Fehlern in der Produktionsumgebung können Hotfix-Branches erstellt werden, um die Fehler zu isolieren und schnell zu beheben. Die behobenen Änderungen können dann in den Produktionszweig gemerged werden, um die Fehlerbehebung zu implementieren.</li>
    <li><strong>Code-Reviews und Qualitätssicherung:</strong> Durch das Erstellen von separaten Branches für Code-Reviews und Qualitätssicherung können Teams Änderungen isoliert überprüfen und sicherstellen, dass nur qualitativ hochwertiger Code in die Hauptentwicklungslinie integriert wird.</li>
</ul>

<h2>Wie nutzt man Git Branching und Merging in einem Projekt? (Schritt-für-Schritt-Anleitung)</h2>
<ol>
    <li><strong>Erstellen eines neuen Branches:</strong> Verwenden Sie den Befehl <code>git checkout -b [Branchname]</code>, um einen neuen Branch zu erstellen und zu diesem zu wechseln.</li>
    <li><strong>Arbeiten im Branch:</strong> Führen Sie die gewünschten Änderungen im neuen Branch durch und committen Sie sie mit <code>git commit -m "Commit-Nachricht"</code>.</li>
    <li><strong>Merging von Branches:</strong> Wechseln Sie zum Zielbranch, den Sie mit dem neuen Branch mergen möchten, und führen Sie den Befehl <code>git merge [Branchname]</code> aus.</li>
    <li><strong>Beheben von Merge-Konflikten:</strong> Wenn es Merge-Konflikte gibt, bearbeiten Sie die betroffenen Dateien manuell, um die Konflikte zu lösen, und führen Sie dann <code>git add</code> und <code>git commit</code> aus, um die Änderungen zu übernehmen.</li>
    <li><strong>Löschen von Branches:</strong> Nachdem der Branch erfolgreich gemerged wurde, können Sie ihn mit <code>git branch -d [Branchname]</code> löschen, um die Arbeitsumgebung sauber zu halten.</li>
</ol>

<p>Indem Sie Git Branching und Merging in Ihrem Projekt verwenden und die oben genannten Schritte befolgen, können Sie einen strukturierten Entwicklungsprozess gewährleisten und eine effiziente Zusammenarbeit im Team fördern.</p>
`;

const textArray = [keinText, textJira, textGitBranchingMerging];

function update(i) {
  //console.log(technologiesBody);
  technologiesBody.innerHTML = textArray[i];
}
